# Ensure that bin and obj directories exist.
$(shell mkdir -p bin)
$(shell mkdir -p obj)
$(shell mkdir -p doc)

FC = gfortran
FFLAGS = -O3 -Jobj
EXE = main
TESTEXE = test
SRCDIR = src
OBJDIR = obj
BINDIR = bin
TESTDIR = test
MAINOBJ = $(EXE).o

# Find all .f90 files in src, create macro with corresponding objects.
SOURCES := $(wildcard $(SRCDIR)/*.f90)
OBJECTS := $(SOURCES:$(SRCDIR)/%.f90=$(OBJDIR)/%.o)

# Same idea for test files.
TESTSOURCES := $(wildcard $(TESTDIR)/*.f90)
TESTOBJECTS := $(TESTSOURCES:$(TESTDIR)/%.f90=$(OBJDIR)/%.o)
# test.f90 defines its own main, so we filter main.o out of this macro.
TOBJ_NOMAIN := $(TESTOBJECTS) $(filter-out $(OBJDIR)/$(MAINOBJ), $(OBJECTS))

# Main program dependencies and rule
$(EXE): Makefile $(OBJECTS) .depend
	$(FC) $(FFLAGS) -o $(BINDIR)/$@ $(OBJECTS)

# Test dependencies and rule.
$(TESTEXE): $(TOBJ_NOMAIN) Makefile .depend
	$(FC) $(FFLAGS) -o $(BINDIR)/$@ $(TOBJ_NOMAIN)

depend: .depend

# There is a problem here, as documented on http://gcc.gnu.org/bugzilla/show_bug.cgi?id=49149
# It still works, but the makefile might have to be re-run to make everything correctly.
# The developers do not seem interested in fixing this. Additionally, the command to make
# the dependencies also fills the root directory with .mod files. It is possibly more trouble
# than it is worth.
.depend: cmd = gfortran -cpp -MM -MF depend $(var); cat depend >> .depend;
.depend:
	@echo "Generating dependencies"
	@$(foreach var, $(SOURCES) $(TESTSOURCES), $(cmd))
	@rm -f depend
	@mv *.mod obj

# Rule all to make main and test.
all: $(EXE) $(TESTEXE)

# In general, how to make .o from .f90 and place it in the right directory.
$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.f90
	$(FC) $(FFLAGS) -c $< -o $@

# Same idea but for the test sources.
$(TESTOBJECTS): $(OBJDIR)/%.o : $(TESTDIR)/%.f90
	$(FC) $(FFLAGS) -c $< -o $@

# Make the documentation
doc: .PHONY
	doxygen doxyfile

.PHONY:

# Remove everything.
clean:
	rm -f $(BINDIR)/* $(OBJECTS) $(TESTOBJECTS) $(OBJDIR)/*.mod .depend

# When the parameter given to make is not 'clean', include the dependencies.
# Stops them being generated only to be cleaned up.
ifneq ($(MAKECMDGOALS),clean)
-include .depend
endif

