!> Unit test to test the make_halo() subroutine found in the 
! grid.f90 module.
module make_halo_unit_test

    use grid
    
    implicit none

    integer :: i,j
    integer, parameter :: n=5, land = 1
    integer, dimension(:,:), allocatable :: terrain

contains

    subroutine make_halo_test()

        !!Assign the terrain to land. 
        allocate(terrain(n, n))
        terrain = land

        !!Check input file with output to terminal.

        !!Check each dimension is correct before halo.
        write(*,*),"(Before)1st Dimension: ",size(terrain,1)
        write(*,*),"(Before)2nd Dimension: ",size(terrain,2)

        !!Check that the array is all ones.
        do i=1,size(terrain,1)

           write(*,"(1x,1000g10.5)")(terrain(i,j),j=1,size(terrain,2))

        end do

        write(*,*),"Make_halo() subroutine call."

        call make_halo(terrain)

        !!Check each dimension is correct after halo.
        write(*,*),"(After)1st Dimension: ",size(terrain,1)
        write(*,*),"(After)2nd Dimension: ",size(terrain,2)

        !!Check that the array has a halo.
        do i=1,size(terrain,1)

           write(*,"(1x,1000g10.5)")(terrain(i,j),j=1,size(terrain,2))

        end do

    end subroutine make_halo_test

end module make_halo_unit_test


