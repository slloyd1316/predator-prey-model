!> Unit test used to test the subroutine called init() 
!! from the initialise.f90 module. 
module init_unit_test

    use initialise

    implicit none 

    integer :: i,j
    integer, parameter :: n = 5
    type(grid_points) :: world

contains
  
    subroutine init_test()

        allocate(world%predators(n,n))
        allocate(world%prey(n,n))
        allocate(world%terrain(n,n))

        !!Set the land.
        world%terrain(:,:) = 1

        !!Call init() sub routine.
        call init(world)

        !!Check call with output to terminal.

        !!Check that the population densities have been initialised between 0.0 and 5.0 .
        write(*,*)"Predator densities:"

        do i=1, size(world%predators,1)
           write(*,"(1x,1000g10.5)")(world%predators(i,j),j=1,size(world%predators,2))
        end do

        write(*,*)"Prey densities"

        do i=1, size(world%prey,1)

           write(*,"(1x,1000g10.5)")(world%prey(i,j),j=1,size(world%prey,2))

        end do

    end subroutine init_test

end module init_unit_test
