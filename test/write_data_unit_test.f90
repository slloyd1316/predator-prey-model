!> Unit test used to test the subroutine called write_data() 
!! from the output.f90 module. 
module write_data_unit_test

    use output
  
    implicit none

    !!Initialise a 2D array with predator and prey population densities along with
    !surface types.
    type(grid_points) :: world
    character(30) :: output_file = "write_data_test_output.dat"
    integer, parameter :: n = 200, land = 1, water = 0
    integer :: i,j
    
contains

subroutine write_data_test()

  allocate(world%predators(n,n))
  allocate(world%prey(n,n))
  allocate(world%terrain(n,n))
  
  do i=1, n
    do j=1, n
      !!Set prey and predator densities.
      world%prey(i,j) = i*j
      world%predators(i,j) = i*j*i*j 
    end do
  end do

  !!Set the terrain and halo.
  world%terrain(1:,1:) = water
  world%terrain(size(world%terrain,1):, size(world%terrain,2):) = water
  world%terrain(2:(size(world%terrain,1)-1), 2:(size(world%terrain,2)-1)) = land

  !!Call write_data() with array.
  call write_data(output_file, world)

  !!Check output file with input above.

end subroutine write_data_test

end module write_data_unit_test
