!> Unit test used to test the subroutines called load_parameters() 
!! and set_params() from the load.f90 and update.f90 modules. 
module load_parameters_unit_test

    use load
    use grid
    use update

    implicit none

    character(30) :: param_input = "parameters.dat"
    type(world_params) :: parameters
    integer, dimension(5,5) :: terrain
    
contains 

    subroutine load_parameters_test()

        write(*,*)"Calling load_parameters()..."
        call load_parameters(param_input, parameters)

        !!Check that terminal output matches data in parameters.dat.
        write(*,*)"Parameters from input file:",param_input
        write(*,*)"r:",parameters%r  
        write(*,*)"a:",parameters%a 
        write(*,*)"b:",parameters%b
        write(*,*)"m:",parameters%m 
        write(*,*)"k:",parameters%k
        write(*,*)"l:",parameters%l
        write(*,*)"Time step:",parameters%time_step
        write(*,*)"Frequency:",parameters%freq
        write(*,*)"Iterations:",parameters%iterations
        write(*,*)"Map file:",parameters%map_file

        !!Check that parameters have been set correctly in update module.
        write(*,*)"Calling set_parameters()..."
        call set_params(terrain,parameters)

        write(*,*)"r:",r  
        write(*,*)"a:",a 
        write(*,*)"b:",b
        write(*,*)"m:",m 
        write(*,*)"k:",k
        write(*,*)"l:",l
        write(*,*)"Time step:",time_step

        !!Deallocate land_neighbours in module so that it can be used in other
        !unit tests.
        deallocate(land_neighbours)

    end subroutine load_parameters_test

end module load_parameters_unit_test
