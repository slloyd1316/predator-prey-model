!> Integration test to the code all together, specifying 
!explicit population densities.
module integration_test
    
    use grid
    use load
    use initialise
    use update
    use output
    use population_counter

    implicit none

    integer :: i, j, time, print_freq
    integer, parameter :: filenumber = 123
    character(30) :: param_input = "test_parameters.dat", terrain_file, output_file
    type(grid_points) :: world
    integer, dimension(:,:), allocatable :: terrain
    type(world_params) :: parameters
    real,parameter :: prey_density = 0.5, predator_density = 0.5

contains 

    subroutine main_test()

        call load_parameters(param_input, parameters)

        terrain_file = parameters%map_file

        call load_terrain(terrain_file, terrain)
        call make_halo(terrain)

        allocate(world%terrain(size(terrain, 1), size(terrain, 2)))
        allocate(world%predators(size(terrain, 1), size(terrain, 2)))
        allocate(world%prey(size(terrain, 1), size(terrain, 2)))

        world%terrain(:,:) = terrain(:,:)

        !Set the densities.
        where (world%terrain == 1)
           world%predators = 1.0
           world%prey = 0.0
        end where

        world%prey(size(world%prey,1)/2,size(world%prey,2)/2) = 1.0

        write(output_file, '(''life'', i3.3, ''.ppm'')') 0

        call write_data(output_file, world)

        call set_params(world%terrain, parameters)
        print_freq = parameters%freq
        time = parameters%iterations

        ! Loop that calls iterate() and write_data() subroutines
        ! for a given length of time.
        do i = 1, time
           call iterate(world)
           call update_popcount(i, world%prey, world%predators)
           ! Only prints when i is a multiple of print_freq.
           if(mod(i, print_freq) == 0) then
              write(output_file, '(''life'',i3.3, ''.ppm'')') i
              call write_data(output_file, world)
           end if
        end do
        call finalise()

    end subroutine main_test

end module integration_test
