!> Unit test used to test the subroutine called update_pop_count() 
!! from the population_counter.f90 module. 
module update_popcount_unit_test

    use population_counter

    implicit none

    integer, parameter :: num_iterations = 20
    integer, parameter :: length = 5
    integer :: t
    real, dimension(length,length) :: prey_array, pred_array
            
contains

    subroutine update_popcount_test()

        !!Open file to write to called "population.txt".
        call init_popcount()

        !!Initialise arrays.
        prey_array = 0.0
        pred_array = 0.0

        !!Loop over iterations settting values arrays.
        do t=1, num_iterations 

           call update_popcount(t,prey_array,pred_array)
           prey_array = t
           pred_array = t

        end do

    end subroutine update_popcount_test

end module update_popcount_unit_test
