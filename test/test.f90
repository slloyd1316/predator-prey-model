!> Program that calls each unit test in a subroutine 
! along with one integration test.
program test

    use iterate_unit_test
    use init_unit_test
    use load_terrain_unit_test
    use load_parameters_unit_test
    use make_halo_unit_test
    use update_popcount_unit_test
    use write_data_unit_test
    use integration_test

    implicit none

    write(*,*)"Starting iterate test..."
    call iterate_test()

    write(*,*)"Starting initialisation test..."
    call init_test()

    write(*,*)"Starting load terrain test..."
    call load_terrain_test()

    write(*,*)"Starting load parameters test..."
    call load_parameters_test()

    write(*,*)"Starting make halo test..."
    call make_halo_test()

    write(*,*)"Starting update popcount test..."
    call update_popcount_test()

    write(*,*)"Starting write data test..."
    call write_data_test()

    write(*,*)"Starting integration test..."
    call main_test()

end program test
