!> Unit test used to test the subroutine called load_terrain() 
!! from the load.f90 module. 
module load_terrain_unit_test

    use load
  
    implicit none

    integer :: i,j
    integer, dimension(:,:), allocatable :: terrain
    character(30) :: terrain_image = "small.dat"

contains

    subroutine load_terrain_test()

      !!Call load_map() with array.
      call load_terrain(terrain_image, terrain)

      !!Check input file with output to terminal.
      
      !!Check each dimension is correct.
      write(*,*),"1st Dimension: ",size(terrain,1)
      write(*,*),"2nd Dimension: ",size(terrain,2)

      !!Check that the array is the same as the input file.
      do i=1,size(terrain,1)

         write(*,"(1x,1000g2.5)")(terrain(i,j),j=1,size(terrain,2))

      end do

    end subroutine load_terrain_test

end module load_terrain_unit_test
