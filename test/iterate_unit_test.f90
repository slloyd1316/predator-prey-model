!> Unit test to test the subroutine called iterate()
!! found in the module update.f90. 
module iterate_unit_test

    use update

    implicit none
    
    !!Initialise a 2D array with predator and prey population densities along with
    !!surface types.
    type(grid_points) :: world
    type(world_params) :: parameters
    integer :: i,j,t
    integer, parameter :: land = 1, water = 0, n = 5, num_iterations = 10
    real, parameter :: prey_density = 0.5, predator_density = 0.5       
 
contains

    subroutine iterate_test()
        
        allocate(world%predators(n,n))
        allocate(world%prey(n,n))
        allocate(world%terrain(n,n))
        
        !!Set prey and predator densities.
        world%prey(:,:) = prey_density
        world%predators(:,:) = predator_density

        !!Set the terrain.
        world%terrain(:,:) = land

        !!Sets parameters in the update.f90 module.
        parameters%r = 0.08  
        parameters%a = 0.04
        parameters%b = 0.02
        parameters%m = 0.06
        parameters%k = 0.2
        parameters%l = 0.2
        parameters%time_step = 0.4

        !!Set the parameters for the update scheme.
        call set_params(world%terrain, parameters)

        !!Check results of each iteration in terminal.
        do t=1, num_iterations

           write(*,*)"Iteration:",t

           write(*,*)"Predator densities:"

           do i=1, size(world%predators,1)
              write(*,"(1x,1000g10.5)")(world%predators(i,j),j=1,size(world%predators,2))
           end do

           write(*,*)"Prey densities"

           do i=1, size(world%prey,1)

              write(*,"(1x,1000g10.5)")(world%prey(i,j),j=1,size(world%prey,2))

           end do

           call iterate(world)

        end do

        !!Deallocate land_neighbours in module so that it can be used in other
        !unit tests.
        deallocate(land_neighbours)

    end subroutine iterate_test

end module iterate_unit_test
