\section{Programming Language}
\label{sec:language}

The choice of language strongly directed the path that development took.  It was an important choice that was made based on the experience of the members of the group, and more importantly the problem at hand.  The program was written in {\tt Fortran}, using as many modern features of the language as possible.

Advantages:

\begin{itemize}

    \item Good array handling
    \begin{itemize}
        \item The main advantage for this project.  Several large arrays which are very easy to manipulate with nice array syntax.
        \item Intuitive handling of multidimensional arrays.  Useful here considering the project specified the need for 2D floating point arrays.  No need to use arraloc in the case of {\tt C}.
        \item Produces very readable code, particularly for update loops with where statements and cshifts being used extensively.  
        \item Was used for several intrinsic elemental array operations which, being intrinsic, reduce code size and the need for extra dependencies, and are highly optimised.
    \end{itemize}

    \item Well optimised for numerical applications
    \begin{itemize}
        \item Used in high performance computing, consequently pushing for well optimised numerical libraries and compilers.
        \item This is quite a computationally heavy project, particularly at the array sizes (up to 2000x2000) suggested in the project specification.  Therefore require code to be as optimised as possible.
        \item Has a simple and sufficient intrinsic random number generation scheme~\cite{rng}.  Uses the KISS (``Keep it Simple Stupid'') pseudo-random number generator.~\cite{kiss}  It utilises three separate techniques to achieve an overall period exceeding $2^{123}$ and is OpenMP thread safe.  This is more than enough for arrays of our size.
    \end{itemize}

    \item Well supported parallel extensions
    \begin{itemize}
        \item Although parallelism has not been successfully explored for this project, having co-arrays as standard since 2008~\cite{f2008}, as well as wide support for MPI/OpenMP extensions, along with the {\tt High Performance Fortran} (HPF) standard means that it is something that could easily be exploited for extra performance.
    \end{itemize}

    \item Well established
    \begin{itemize}
        \item Being a long established language does have its disadvantages.  Large amounts of the {\tt Fortran} standard are dedicated to maintaining backwards compatibility with previous iterations.  We can exploit the fact that the language is well documented, well supported and stable.
    \end{itemize}

\end{itemize}


Disadvantages:

\begin{itemize}

    \item File I/O
    \begin{itemize}
        \item Unfortunately, still has poor handling of file input and output.  Doesn't seem to have changed much since {\tt Fortran 77} which results in very unidiomatic ways of reading from, and writing to files.
        \item Results in largely unreadable and bloated code.
        \item Appears unintuitive to developers not well versed in old {\tt Fortran}.
        \item Lacking features that are common in {\tt C} and {\tt Java}.
    \end{itemize}

    \item String handling
    \begin{itemize}
        \item Very limited features for string handling.  Was used slightly when dealing with file names when doing I/O, for which the other languages have more idiomatic approaches.
    \end{itemize}

    \item Standard features only supported by some proprietary compilers.
    \begin{itemize}
        \item The GNU {\tt Fortran} compiler seems less supported and less influential on the standard than its {\tt C} counterpart.
        \item Results in some modern {\tt Fortran} features that would have been very useful for this project, such as unlimited polymorphic types, not being supported by open source compilers.~\cite{gfortranOOP}
        \item We would like to maintain open source compatibility, both out of principle, and to maintain as much portability as possible.
    \end{itemize}

\end{itemize}


Ultimately, a choice was made between {\tt Fortran} and {\tt C}.  We could not justify the performance hit associated with {\tt Java}, because its primary strength of handling GUIs was not going to be used.  The project was also not sufficiently large as to warrant much object orientation.  The choice between the remaining two was much closer.  {\tt Fortran} offered much nicer array syntax whilst {\tt C} provided slightly nicer file I/O.  In the end, a cleaner and more intuitive computational kernel was favoured.  {\tt Fortran} provided array syntax and numerical libraries that enabled this.
