\section{Design}
\label{sec:design}

One of the most important decisions made as a development group was that of code design.  A good modular design promoted an intuitive data structure and code flow.  Having distinct code modules provided the group with a sensible way of delegating tasks, as mentioned in section~\ref{sec:assignment} on page~\pageref{sec:assignment}.  
The main philosophy behind the design was to identify abstract routines, patterns and quantities that are common to scientific simulation, such that the modules they reside in can be instantly identified and understood as a developer.
Consequently, the module files are named after the common task they are assigned to.

Taking a top-down approach, the main program was written first.  This essentially outlined what functionality the modules needed to provide for the code to work, and gave an idea of what tests needed to be written. 

The code was originally structured slightly differently.
The main difference was moving the I/O away from main into its own module and subroutines.  
This keeps code size down in the main program and avoids having to rewrite certain I/O procedures during testing.  The other change was separating initialisation routines from update ones and having separate modules for each.  This distinction was made because it seemed intuitive from a testing and developing point of view when locating the subroutines.

For more in-depth details regarding the code, there is compiled {\tt doxygen} code documentation in the {\tt \/doc} directory.

\subsection{{\tt grid.f90}}
\label{sec:grid}

This module contains the main data structures used in the simulation as well as subroutines used to handle them.  
The module defines the derived data type {\tt grid\_points}.  
The type contains an integer array map of the terrain as well as the two 2D floating point arrays storing the population densities of the predators and prey.  
The simulation requires functionality for calculating the number of neighbouring land grid points, as well as predator and prey populations, at every point on the grid.  These were identified as being the same operation, a summation, in both the (integer) land and (floating point) population case.  Therefore it was deemed possible to write a single polymorphic subroutine that could be used in both cases.  
Originally, the design choice taken was to use an unlimited polymorphic object, such that it is possible to declare a subroutine like so:

\lstset{
        language=Fortran,
        morekeywords={*class(*), class}
        }

\begin{lstlisting}
class(*) sum_neighbours(array, neighbours)
\end{lstlisting}


This is a relatively new feature, introduced in {\tt Fortran 2003} and despite being listed as standard is not universally implemented in the majority of compilers.~\cite{gfortranOOP}  
Ultimately, it was decided that this feature was not significant enough to determine the choice of compiler alone, as discussed in more detail in section~\ref{sec:language} on page~\pageref{sec:language}.  
Instead, an interface was used.  Despite it being a less idiomatic approach, requiring hand coding in the specific use cases ({\tt sum\_integer\_neighbours} and {\tt sum\_real\_neighbours},) it did allow us to write:


\begin{lstlisting}
call sum_neighbours(array, neighbours)
\end{lstlisting}

in both cases.


The module also contains the data type {\tt world\_params}.  This is a simple structure used for packing up the large number of simulation parameters, mainly for readability purposes when passing data between modules and subroutines.


\subsection{{\tt load.f90}}
\label{sec:load}

The module load is tasked with reading data from files. It has a method for
reading the parameters of the simulation.  Such parameters include the predation rate, the prey
growth rate and so on.

The first method is very simple. It opens the file and reads, in fixed order,
the parameters that drive the program. A sample file is included to make it easy
for the user to create their own data files. The program expects any file passed
to it to conform to the sample file distributed with the code and will not work
if the provided files do not. It was felt that if the data the program reads is
erroneous, there is not a great deal that can be done, so when there is an issue
the {\tt Fortran} runtime exits. The data read from the file is read into a derived
data type for convenience as it is easier to move one structure around the
program than it is several small variables which are all used in the same place.

The second subroutine in the file reads the terrain data from the file specified
in the parameter file. The format it expects is exactly the same as detailed in
the specification. It first reads the dimensions of the file then uses a custom
format string to read that many integers from each line of the file.


\subsection{{\tt initialise.f90}}
\label{sec:initialise}

This module sets up the initial population densities, given a certain terrain, for the simulation.
It randomly assigns each grid point a density for predator and prey populations between 0.0 and 5.0, using the {\tt Fortran} intrinsic random number subroutine:~\cite{rng}

\begin{lstlisting}
call random_number()
\end{lstlisting}

A feature that was often exploited in the project was the use of {\tt Fortran} array syntax.  As discussed in section~\ref{sec:language} on page~\pageref{sec:language}, it was an important reason in picking {\tt Fortran} as a language for the project.  
However, in order to use it in this case, {\tt random\_number()} had to be used to populate entire arrays with random numbers before entering the {\tt where} statement:

\begin{lstlisting}
! These arrays are filled with random numbers
call random_number(random_predators)
call random_number(random_prey)
! The values from the previous arrays are only used where there is land
where (grid%terrain == 1)
    grid%prey = 5.0*random_prey
    grid%predators = 5.0*random_predators
end where
\end{lstlisting}

This means random numbers are being generated for grid points where there is water, where they will not be used.  The alternative would be losing the nice syntactic sugar of array syntax in place of loops.  
This would allow us to call {\tt random\_number()} on an element by element basis.  However, as will be seen in section~\ref{sec:performance} on page~\pageref{sec:performance}, profiling showed that initialisation took a negligible amount of time within the simulation, even for large arrays.  Therefore, the overhead associated with generating extra unused random numbers was small enough to justify keeping nice array syntax.

\subsection{{\tt update.f90}}
\label{sec:update}

The update module contains the main computational kernel for the program.  This was the first module to be written.  
The idea behind doing so was that a computational kernel should be as `mathematically pure' as possible.
In other words, it should know as little about the inner workings of the program it was written for as possible.
Whilst this philosophy is true for most modules, in order to promote reusability and portability, it is particularly important for writing something that is the core of the program that should be able to be interfaced by several features such as I/O or GUIs etc.

Several opportunities for abstraction were identified in this module. One was to identify that predator and prey both update according to essentially the same differential equations, but with different parameters.  Another was to notice that both populations diffused in the same fashion.  By identifying such abstractions, single subroutines were written for each, rather than rewriting essentially the same code for both predators and prey.

\subsection{{\tt population\_counter.f90}}
\label{sec:populationcounter}

This module is very simple. It has methods for opening and closing
the file that the population data is written to as opening and closing it every
iteration would be unnecessarily expensive. The {\tt update\_popcount()} subroutine is
called every iteration by main and uses {\tt Fortran} intrinsic functions to calculate
the total populations of the predators and prey.

\subsection{{\tt output.f90}}
\label{sec:output}

The output module has only one task and that is to output the full state of the
grid at a time {\tt t}. It calculates the maximum population density across the whole
grid, then takes the maximum of those two as a normalising factor; this is later
used to calculate the intensity of the colour at a grid point. The header
information is written and then the arrays are looped over to obtain the density
values. The data is normalised then scaled by 255 to
bring it within a range suitable for display. The data is looped over in the
column index first to ensure local data access and that the data is printed in
the correct orientation. The colours produced are a mix of red and green for the
land areas with the relative intensities of each representing the density of
each population.
