\section{Performance and Analysis}
\label{sec:performance}

\subsection{Compiler Optimisation Flags}
\label{sec:compilerflags}

During the development phase of the project, compiler optimisation flags were switched off by default.~\cite{oflags}  After development, the code was debugged and tested rigorously, in order to ensure that the program worked as expected with zero optimisation.  Optimisation levels for the gfortran compiler were then stepped up from -O1 through -O3, with retesting done at each step to ensure the code was still working as intended.
No errors were found at any of these optimisation levels.  The results of testing the effects each optimisation level had on run time are summarised in Table \ref{tab:flags}.

\begin{table}[h]
\centering
\begin{tabular}{| l | c | r |}
\hline
Optimisation Level & Run time (s) & Speedup \\ \hline
None & 8.88 & 0\% \\ \hline
O1 & 6.75 & 31.5\% \\ \hline
O2 & 6.39 & 39.0\% \\ \hline
O3 & 6.26 & 41.9\% \\ \hline
\end{tabular}
\caption{Shows the effect of compiler optimisation flags on runtime.}
\label{tab:flags} 
\end{table}
 

Significant speed up was obtained from turning on initial O1 optimisation.  This is to be expected as the common, none speed-space trade off, optimisations the compiler can do are turned on.  Because of this, the executable size did not change.  Moving up to O2, instruction scheduling flags are enabled, yielding a small speed up.  Moving to O3 yielded next to no speedup.  This is because the program was not large enough such that speed-space optimisations, like function inlining, become significant.  This contributed very slightly to the size of the executable.  The relatively small size of the project meant that the extra memory usage required by the compiler to carry out the varying levels of optimisation were negligible, resulting in no noticeable increase in compilation time.  Unless the size of the executable became unmanageable, the code would be shipped with O3 enabled.
 

\subsection{Profiling} 
\label{sec:profiling}

The code's performance was analysed mainly by profiling. In light of this premature optimisation was avoided as attempts to speed up the code were only made where it would have an actual noticeable effect.  As will be shown, the results were often surprising, resulting in areas of optimisation that would have not been explored otherwise.  Several improvements were made after analysing our program with the GNU profiler, gprof.~\cite{gprof}  Here is a part of the original profile of the code, running for 100 iterations with no output, on the {\tt islands.dat} terrain map\footnote{The machines in the HPC workroom 3305 were used for testing purposes. 3.2GHz Intel Pentium 4, 1GB RAM, Scientific Linux v6.3, gfortran 4.4.6 -O3 tag enabled.  All results were taken as an average of 3 independent runs.}:

\lstset{
        language=
       }

\begin{lstlisting}

 \%      self         total           
 time   seconds     s/call     name    
 33.62     4.86       0.05     __update_MOD_iterate
 21.24     3.07       0.02     integer_sum_neighbours_
 20.55     2.97       0.01     real_sum_neighbours_
  8.79     1.27       0.01     __...update_population
  7.26     1.05       1.05     MAIN__
  5.81     0.84       0.00     __update_MOD_diffuse
                ...
\end{lstlisting}

As seen above, a large amount of time was spent in the {\tt iterate()} subroutine of the update module.  
Upon realisation that this does not include the time spent within subsequent sub-subroutine calls, it was apparent that something was wrong.  
The {\tt iterate()} subroutine in itself appeared to do very little work.  It simply made deep copies of the population arrays, updated the input parameters as well as the number of land neighbours, and called the {\tt update\_population()} routine with these deep copies.  
It was these sub calls which delegated the actual, significant, computation.  Below is a description of some of the changes made, followed by a table comparing the relative speed ups obtained from the output profile.

First it was identified that only one deep copy needed to be made, to the prey, as this was the population to be updated first.  Even removing deep copies all together (and consequently losing concurrent, correct results) made very little difference to the run time of the program.  This change was consequently left out of Table \ref{tab:optimisations}.

The only thing left that caused obvious suspicion was that it was unnecessarily updating certain parameters each step.  It was assumed that the compiler, with an optimisation level of at least 1 (hence activating {\tt -fmove-loop-invariants},) would move such operations outside of the loop.  However, it appears that there was a slight speedup by moving these out of the update loop and in to its own, called once, subroutine {\tt set\_params()} by hand.   
In the same step of optimisation, the recalculation of the number of land neighbours at each step was also deemed unnecessary.  
Obviously, the map does not change throughout the simulation, so this was also moved in to the {\tt set\_params()} subroutine.  The size of the array clearly made it sufficiently difficult, or inefficient, for the compiler to check if it changes at each iteration in the loop.  The speedup here did not affect the run time spent in the {\tt iterate()} subroutine itself, because the computation was done by {\tt integer\_sum\_neighbours()}.  The speedup was significant, and is seen in Figure \ref{tab:optimisations}, but still did not solve our original problem.

With no real obvious computation left in the subroutine that could be the cause of the slow runtime, the way it handled memory was then investigated.  It was observed that using smaller terrain maps made a significant difference to the time spent in the subroutine, more so than the expected n$^2$ complexity associated with the numerical algorithms implemented in the program.  
Originally, the data type {\tt grid\_points} was named {\tt grid\_point} and contained information on, as the name says, a single grid point, rather than arrays of points.  It was proposed that by having an array of a derived data type meant that Fortran was internally reconstructing arrays every time {\tt array\%type} was called.  This means that at every iteration, the predator and prey population densities in the large array of derived data type {\tt grid\_point} were being moved in to their own arrays every time they were referenced like above.  For small sized arrays, this was not noticeable and were most likely moved in cache.  As the size of the arrays increased, this quickly became a problem.  The hypothesis was confirmed when the code design was changed to accommodate a single data type of arrays, {\tt grid\_points}, rather than an array of {\tt grid\_point}.  As shown in Table \ref{tab:optimisations}, the speedup was huge and meant that {\tt iterate()} was no longer a significant source of overhead.

\begin{table}[h]
\centering
\begin{tabular}{| l | c | p{2.5cm} | p{2.5cm}| }
\hline
optimisation & iterate (s) & integer sum neighbours (s) & total run time (s) \\ 
\hline
no optimisation & 4.86 (33.62\%) & 3.07 (21.24\%) & 14.47 \\
\hline
moved loop invariants & 4.46 (38.73\%) & 0.01 (0.09\%) & 11.52 \\
\hline
derived arrays & 0.38 (7.23\%) & 0.01(0.19\%) & 5.26 \\
\hline 
\end{tabular}
\caption{Shows the effect of various optimisations on runtime.}
\label{tab:optimisations}
\end{table}
  


Although moving loop invariants outside of the update loop by hand was a significant optimisation, cutting run time by 2.95s for just 100 iterations, it was the change of {\tt grid\_points} that yielded the most speedup (5.26s).

Turning on I/O significantly affected runtime as expected.  For a fair comparison, the program was set to print to file every iteration.  Realistically, this is unnecessary and done just for testing purposes.  Again, running on the same machine for 100 iterations, with the above discussed optimisations applied, the following profile was obtained:

\begin{lstlisting}

 \%	    self      total
 time      seconds    s/call     name
 57.96      7.30      0.07     __output_MOD_write_data
 21.60      2.72      0.01     real_sum_neighbours_
  8.02      1.01      0.01     __...MOD_update_population
  7.70      0.97      0.00     __update_MOD_diffuse
  3.33      0.42      0.00     __update_MOD_iterate
  1.11      0.14      0.00     __...MOD_update_popcount
  0.16      0.02      0.02     integer_sum_neighbours_

\end{lstlisting}

With disc access being slow, and Fortran I/O optimisations largely being out of the hands of the programmer, it was somewhat reassuring that the program was spending the majority of its time printing to file.  Even with a realistic print frequency, a significant amount of time is spent doing this.  Further numerical optimisations were found.  Even so, any realistic computational optimisation would no longer have made a significant difference to the run time.

\subsection{Performance Analysis Results}
\label{sec:analysisresults}

With optimisations in place, the performance of the program for various cases was then investigated.  An exploration of the effects of different sized maps was made, as well as different proportions of land and water.  The results are summarised in Figure \ref{fig:mapanalysis}.

\begin{figure}[h]
\centering
\includegraphics[scale=0.5]{mapanalysis.png}
\caption{Plot showing the effect terrain size, N x N, has on simulation run time for various maps.  All values are an average of 3 runs, running on the same machines as described above, for 100 iterations.  Printing to map every 10 iterations.}
\label{fig:mapanalysis}
\end{figure}

As seen in Figure \ref{fig:mapanalysis}.  The run time scales approximately by N$^2$.  For example, when N doubles, the run time goes up roughly by a factor of 4 for all map cases.  This is to be expected as the neighbour summing and printing routines are all $O(N^2)$ algorithms.  The different plots were used to explore both the effect of having more land (and hence more numerical calculation,) and of branch prediction.  

As shown by comparing the `all land' and `all water' plots above, larger amounts of land adds to the run time.  Perhaps slightly less obvious is the effect of how this amount of land is distributed.  The two plots `half and half' and `checkerboard' take 50\% land and distribute it on the map differently.  The top half of the map in `half and half' is land whilst the bottom half is water.  The `checkerboard' plot shows what happens when land and water is distributed in a checkerboard fashion.  The checkerboard map has a significantly longer run time than the half and half map.  This will most likely be due to how the branch prediction works when determining whether a point is land or water.  Whenever the statement

\lstset{
        language=Fortran
        }

\begin{lstlisting}
! Where there is land
where (grid%terrain == 1)
\end{lstlisting}

is accessed, branch prediction will try to predict the outcome for an element in the array, and carry on with the consequent computation, or move on to the next element of the array.  This is easy to see when seeing where statements as syntactic sugar for what is essentially an if statement nested in D do loops, where D is the number of dimensions of the array.  A checkerboard will cause the branch prediction to guess incorrectly and therefore waste cycles on unnecessary calculation for that element of the array.  In contrast, the branch predictor will guess correctly in the `half and half' case for the majority of elements.  If it assumes the branch returns true, it will guess the first element correctly.  If it is single bit branch prediction, it will guess incorrectly only once, when the half way point in the array is accessed.  If it is two bit branch prediction, it will guess incorrectly twice at this boundary.  This is reflected in the quicker run time.  

\subsection{Parallelism}
\label{sec:parallelism}

The code branch feature of Git was used to create a version of the code which
utilised multiple threads. The subroutine {\tt real\_sum\_neighbours()}
was identified as the function which was most in need of parallelisation to
decrease execution time. A directives-based parallelisation scheme was chosen
because of the ease of adding it to the code. The API that was added was OpenMP.

The subroutine {\tt real\_sum\_neighbours()} mostly uses cshift functions to calculate
the number of neighbours a grid point has. Because of this, the only directive
in OpenMP that can be used is the {\tt WORKSHARE} directive. This does not allow fine-
tuning of the method used to decompose the work in the parallel region but is
the only directive that can be used with array syntax and certain intrinsic
functions.

The code was tested in a parallel environment of up to sixteen cores with
disappointing results. While the execution time remained roughly constant, the
total time spent across all cores increased linearly with the number of cores
used. This seems to indicate that the work was given to each core with no
subdivision of the array across cores. It appears the to fully utilise multiple
threads the code would have to be rewritten without the use of array syntax and
manual subdivision of the arrays, as the compiler cannot do it do it by itself
in this case.
