!> Module for loading files consisting of arrays of ints and floats
!! representing land type and population densities.
module load
    use grid
    implicit none

    !> Integer parameter, ideally a unique file identifier.
    integer, parameter :: fileid = 222

contains

    !> Loads the parameters for the simulation.
    !! @param{parameter_file} the name of the file that contains the parameters
    !! of the simulation
    !! @param{parameters_i} the derived type to place the data that has been
    !! read in.
    subroutine load_parameters(parameter_file, parameters_i)
        type(world_params), intent(out) :: parameters_i
        character(30) :: parameter_file
        open(unit = fileid, file = parameter_file)
        ! All reads done using no format string - allows for comments after
        ! values in the file
        read(fileid, *) parameters_i%r
        read(fileid, *) parameters_i%a
        read(fileid, *) parameters_i%b
        read(fileid, *) parameters_i%m
        read(fileid, *) parameters_i%k
        read(fileid, *) parameters_i%l
        read(fileid, *) parameters_i%time_step
        read(fileid, *) parameters_i%freq
        read(fileid, *) parameters_i%iterations
        ! Will read at maximum 30 characters - avoids overflow but can mangle
        ! filenames
        read(fileid, '(A30)') parameters_i%map_file
        close(fileid)
    end subroutine load_parameters

    !> Loads the terrain data  from file and returns a 2D integer array
    !! representation of the landscape.  1 denotes land and 0 denotes water.
    !! Allocates memory which will need to be deallocated by the caller.
    !! @param map_image String containing the path to the image file
    !! @param terrain 2D integer array representation of the landscape.
    !! 1 denotes land and 0 denotes water
    subroutine load_terrain(map_image, terrain)
        integer, dimension(:,:), allocatable, intent(out) :: terrain
        integer :: M, N
        character(30) map_image, string_format
        open(unit = fileid, file = map_image)
        read(fileid, *) M, N
        ! Write map width into format string, so we know how many ints to read.
        write(string_format, '(''('', i4, ''(i1,1x))'')') M
        allocate(terrain(M,N))
        ! Contains an implicit loop to read N lines from the file, but is nice
        ! and compact.
        read(fileid, string_format) terrain
        close(fileid)
    end subroutine load_terrain
end module

