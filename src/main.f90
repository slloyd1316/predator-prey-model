!> Main entry point for the program. Collects all files and
!! co-ordinates the different modules.
program main
    
    use grid
    use load
    use initialise
    use update
    use output
    use population_counter
    
    implicit none

    integer :: i, time, print_freq
    integer, parameter :: filenumber = 123
    character(30) :: param_input, terrain_file, output_file
    type(grid_points) :: world
    integer, dimension(:,:), allocatable :: terrain
    type(world_params) :: parameters

    ! Must have one argument, a data file providing the simulation
    ! parameters. See the provided file paramters.dat for details.
    call get_command_argument(1, param_input)
    call load_parameters(param_input, parameters)

    terrain_file = parameters%map_file

    call load_terrain(terrain_file, terrain)
    !! Makes a halo of water around the file terrain data.
    call make_halo(terrain)

    !! Allocating space to the three arrays in world.
    allocate(world%terrain(size(terrain, 1), size(terrain, 2)))
    allocate(world%predators(size(terrain, 1), size(terrain, 2)))
    allocate(world%prey(size(terrain, 1), size(terrain, 2)))

    ! Copy data into the world
    world%terrain(:,:) = terrain(:,:)

    ! Initialising code for two modules
    call init(world)
    call init_popcount()

    !! Write out the initial population densities following
    !! the terrain we've just loaded. Initial data goes in life0000.dat
    write(output_file, '(''life'', i4.4, ''.ppm'')') 0
    call write_data(output_file, world)

    ! Set the data for the simulation in the module
    call set_params(world%terrain, parameters)
    print_freq = parameters%freq
    time = parameters%iterations

    ! Run the simulation for as many iterations as are asked
    do i = 1, time
        call iterate(world)
        call update_popcount(i, world%prey, world%predators)
        ! Only prints when i is a multiple of print_freq.
        if(mod(i, print_freq) == 0) then
            write(output_file, '(''life'',i4.4, ''.ppm'')') i
            call write_data(output_file, world)
        end if
    end do
    !! Close file in another module.
    call finalise()
end program main
