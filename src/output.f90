!> Module to output float data into a pgm file.
!! Quite slow, but there's little that can be done about that.

module output
    use grid

    implicit none

    integer, parameter :: fileid = 456

contains

    !> Prints 2D grid into file output_file in .pgm format.
    !! The format is human-readable and is therefore huge.
    !! It colours the tiles as follows: water is blue,
    !! and the other tiles are coloured with a mix of red and
    !! green according to the percent of their populations
    !! relative to the largest population of each across the
    !! whole grid.
    !! @param{output_file} the name of the file to write the data to
    !! @param{world the} state of the simulation at a given step
    subroutine write_data(output_file, world)
        type(grid_points), intent(in) :: world
        character(30) output_file
        real :: largest_prey, largest_pred, maxpop
        integer :: m, n, i, j

        m = size(world%terrain, 1)
        n = size(world%terrain, 2)
        ! Variables used in normalising the populations to choose the intensity
        ! of the colours in the map. Both populations scaled to the same value
        ! as otherwise one can easily be drowned out
        largest_prey = maxval(world%prey)
        largest_pred = maxval(world%predators)
        maxpop = max(largest_prey,largest_pred)

        open(unit = fileid, file = output_file)
        ! Write the header data
        write(fileid, '(''P3'')')
        write(fileid, '(i4,1x,i4)') m, n
        write(fileid, '(i3)') 255

        do j = 1, n
            do i = 1, m
                ! Write the predators in red, the prey in green and blue when
                ! the terrain is 0.
                write(fileid, '(3(i3,1x))', advance='no') &
                    (floor(world%predators(i,j) * 255 / maxpop)), &
                    (floor(world%prey(i,j) * 255 / maxpop)), &
                    ((-1 + world%terrain(i,j)) * (-255))
            end do
            write(fileid, '(/)', advance='no')
        end do
        close(fileid)
    end subroutine write_data
end module output

