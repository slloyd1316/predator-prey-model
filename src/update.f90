!> Module containing the computational kernal for the perdator-prey
!! model.  Implements the discretised Lotka-Volterra equations in 2D.
module update

    use grid

    implicit none

    !! Internal utility functions
    private :: update_population
    private :: diffuse

    real time_step, r, a, k, m, b, l

    integer, dimension(:,:), allocatable :: land_neighbours

contains
    
    !> Sets the parameters used in update loop.  Used before the loop starts to
    !! avoid unnecessary memory writing
    !! @param terrain The water/land distribution
    !! @param params the collection of parameters defining the simulation
    subroutine set_params(terrain, params)
        integer, dimension(:,:), intent(in) :: terrain
        type(world_params), intent(in) :: params 
        time_step = params%time_step
        r = params%r
        a = params%a
        k = params%k
        m = params%m
        b = params%b
        l = params%l
        allocate(land_neighbours(size(terrain, 1), size(terrain, 2)))
        call sum_neighbours(land_neighbours, terrain)
    end subroutine set_params

    !> Updates the 2D array for a single step of the Lotka-Volterra equations
    !! @param grid The grid of terrain and population
    !! densities
    !! @param input_params The simulation parameters of type
    !! [world_params.](\typedef world_params)
    subroutine iterate(grid)
        type(grid_points), intent(inout) :: grid
        real, dimension(size(grid%prey,1), size(grid%prey,2)) :: prey
        ! Take deep copy of population densities to ensure populations are
        ! updated at the same time step
        prey(:,:) = grid%prey
        ! Update each population density, supplying deep copies of the opposing
        ! population to ensure they are updated at the same time step
        call update_population(grid%prey, grid%predators, grid%terrain, time_step, r, a, k)
        call update_population(grid%predators, prey, grid%terrain, time_step, -m, -b, l)
    end subroutine iterate

    !> Calculates the birth and death of a population after a single time step.
    !! @param population Population density to be updated
    !! @param competition Opposing population density
    !! @param terrain Map of land and water
    !! @param time_step
    !! @param birth_rate
    !! @param death_rate Rate of naturally occuring, or predator induced, death
    !! @param diffusion_rate
    subroutine update_population(population, competition, terrain, time_step, birth_rate, &
                                    death_rate, diffusion_rate)
        real, dimension(:,:), intent(inout) :: population
        real, dimension(:,:), intent(in) :: competition
        integer, dimension(:,:), intent(in) :: terrain 
        real, intent(in) :: time_step, birth_rate, death_rate, diffusion_rate
        real, dimension(size(population, 1), size(population, 2)) :: diffusion
        diffusion = 0
        diffusion = diffuse(population, terrain, time_step, diffusion_rate)
        where (terrain == 1)
            population = population + time_step*(birth_rate * population - &
                                                death_rate * population * competition &
                                                + diffusion)
        end where
    end subroutine update_population

    !> Calculates the effect of diffusion on a population density across a
    !! grid
    !! @param population
    !! @param terrain 
    !! @param time_step
    !! @param diffusion_rate
    function diffuse(population, terrain, time_step, diffusion_rate)
        real, dimension(:,:) :: population
        real, dimension(size(population, 1), size(population, 2)) :: diffuse
        integer, dimension(:,:) :: terrain
        real :: time_step, diffusion_rate
        real, dimension(size(population,1), size(population,2)) :: population_neighbours
        diffuse = 0
        call sum_neighbours(population_neighbours, population)
        where (terrain == 1)
            diffuse = diffusion_rate * (population_neighbours - land_neighbours * population)
        end where
    end function diffuse

end module update

