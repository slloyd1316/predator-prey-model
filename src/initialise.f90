!> Module containing the initialisation of the computational kernel for
!! the predator-prey model.  Sets up initial conditions for the
!! discretised Lotka-Volterra equations in 2D.
module initialise

    use grid

    implicit none

contains

    !> Randomly initialises population densities using the
    !! intrinsic RNG scheme in Fortran.
    !! @param grid
    subroutine init(grid)
        type(grid_points), intent(inout) :: grid
        real, dimension(size(grid%terrain,1), size(grid%terrain,2)) :: random_predators
        real, dimension(size(grid%terrain,1), size(grid%terrain,2)) :: random_prey
        grid%predators(:,:) = 0.0
        grid%prey(:,:) = 0.0
        ! See GNU docs: calling with no args sets seed based on current system time.
        call random_seed()
        ! These arrays are filled with random numbers 
        call random_number(random_predators)
        call random_number(random_prey)
        ! The values from the previous arrays are only used where there is land.
        where (grid%terrain == 1)
            grid%prey = 5.0*random_prey
            grid%predators = 5.0*random_predators
        end where
    end subroutine init
end module initialise    
