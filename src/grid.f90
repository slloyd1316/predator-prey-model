!> Module defining a 2D grid used for discretising space in simulation.
!! Contains subroutines which allow easy operations on the 2D floating point and
!! integer arrays that grid contains.

module grid

    implicit none

    !> @typedef{grid_points}
    !! Derived data type which contains 2D arrays for the terrain and population
    !! densities.
    type grid_points
        integer, dimension(:,:), allocatable :: terrain !< Map file.  1 = Land, 0 = Water
        real, dimension(:,:), allocatable :: predators !< Predator population densities
        real, dimension(:,:), allocatable :: prey !< Prey population densities
    end type grid_points

    !> @typedef{world_params}
    !! Derived data type which contains all parameters required for the model.
    type world_params
        real :: r !< Prey birth rate
        real :: a !< Predation rate
        real :: b !< Predator reproduction rate
        real :: m !< Predator mortality rate
        real :: k !< Predator diffusion rate
        real :: l !< Prey diffusion rate
        real :: time_step !< Simulation time step
        integer :: freq !< Frequency that images are printed to file
        integer :: iterations !< Total number of update iterations
        character(30) :: map_file !< The name of the terrain map file
    end type world_params    
    
    !> Interface that allows the user to call sum_neighbours on either a    
    !! floating point array or integer array without having to call individual
    !! subroutines for either.
    !! Would use an infinite polymorphic variable for the array instead, but such
    !! functionality does not exist in most Fortran compilers as of yet, despite it
    !! being a standard feature since 2003.
    interface sum_neighbours

        !> Sums the neighbours of a 2D integer array
        subroutine integer_sum_neighbours(neighbours, array)
            integer, dimension(:,:), intent(inout) :: neighbours
            integer, dimension(:,:), intent(in) :: array
        end subroutine integer_sum_neighbours

        !> Sums the neighbours of a 2D floating point array
        subroutine real_sum_neighbours(neighbours, array)
            real, dimension(:,:), intent(inout) :: neighbours
            real, dimension(:,:), intent(in) :: array
        end subroutine real_sum_neighbours
  
    end interface sum_neighbours

contains
    
    !> Surrounds grid with halo
    !! @param terrain an allocated array of some type. MUST be allocated.
    subroutine make_halo(terrain)
        integer, dimension(:,:), allocatable, intent(inout) :: terrain
        integer :: m, n
        integer, dimension(size(terrain, 1), size(terrain, 2)) :: temp
        temp(:,:) = terrain(:,:)
        m = size(terrain, 1) + 2
        n = size(terrain, 2) + 2
        deallocate(terrain)
        allocate(terrain(m, n))
        terrain(1:, 1:) = 0
        terrain(m:, n:) = 0
        terrain(2:(m - 1), 2:(n - 1)) = temp(:,:)
    end subroutine make_halo    

end module grid

subroutine integer_sum_neighbours(neighbours, array)
    integer, dimension(:,:), intent(inout) :: neighbours
    integer, dimension(:,:), intent(in) :: array   
    neighbours(:,:) = 0
    ! Add above element
    neighbours = neighbours + cshift(array, 1, 1)
    ! Add below element
    neighbours = neighbours + cshift(array, -1, 1)
    ! Add left element
    neighbours = neighbours + cshift(array, -1, 2)
    ! Add right element
    neighbours = neighbours + cshift(array, 1, 2)
end subroutine integer_sum_neighbours

subroutine real_sum_neighbours(neighbours, array)
    real, dimension(:,:), intent(inout) :: neighbours        
    real, dimension(:,:), intent(in) :: array
    neighbours(:,:) = 0.0
    ! Add above element
    neighbours = neighbours + cshift(array, 1, 1)
    ! Add below element
    neighbours = neighbours + cshift(array, -1, 1)
    ! Add left element
    neighbours = neighbours + cshift(array, -1, 2)
    ! Add right element
    neighbours = neighbours + cshift(array, 1, 2)
end subroutine real_sum_neighbours

