!> A small module that counts the total predator and prey population at every
!! timestep. init() and finalise() needed for file opening and closing.
module population_counter
    implicit none

    !> Another integer not in use anywhere else in the program
    integer, parameter, private :: fileid = 1425

contains

    !> Open the file population.txt for writing.
    subroutine init_popcount()
        open(unit = fileid, file = 'population.txt')
    end subroutine init_popcount

    !> Subroutine that does the actual calculation and writes it to the file
    !! population.txt.
    !! @param iteration the number of iterations that have so far passed
    !! @param prey_array a float array representing the prey density
    !! @param pred_array a float array representing the predator density
    subroutine update_popcount(iteration, prey_array, pred_array)
        integer, intent(in) :: iteration
        real, dimension(:,:), intent(in) :: prey_array, pred_array
        write(fileid, *) iteration, sum(prey_array), sum(pred_array)
    end subroutine update_popcount

    !> Utility function to ensure the output file is closed at the end.
    subroutine finalise()
        close(fileid)
    end subroutine finalise
end module population_counter
